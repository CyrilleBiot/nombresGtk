#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
nombreGtk.py
Logiciel pour l'apprentissage des nmbres de 1 à 99
Niveaux Cycle 2, élèves à BEP.
__author__ = "Cyrille BIOT <cyrille@cbiot.fr>"
__copyright__ = "Copyleft"
__credits__ = "Cyrille BIOT <cyrille@cbiot.fr>"
__license__ = "GPL"
__version__ = "1.0"
__date__ = "2021/09/29"
__maintainer__ = "Cyrille BIOT <cyrille@cbiot.fr>"
__email__ = "cyrille@cbiot.fr"
__status__ = "Devel"
"""

# ====================================================
#      Dépendances DEBIAN :  python3-vlc, python3-gi
# ====================================================
import random, vlc, time
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk
import datetime, os.path


class nbGtk(Gtk.Window):

    def __init__(self):

        # Quelques variables pour l'environnement
        if os.path.exists('.git'):
            # On est dans le git
            self.dirBase = './source/'
        else:
            if os.path.isdir('/usr/share/nombresGtk/') == True:
                # Sur install paquet
                self.dirBase = '/usr/share/nombresGtk/'
            else:
                # en dev
                self.dirBase = './'
        print(self.dirBase)

        # Creation rep log
        repertoire_des_logs = '/home/' +  os.environ.get('USER') + '/.primtux/nombresGtk'
        if not os.path.exists(repertoire_des_logs):
            os.makedirs(repertoire_des_logs)
            print("Répertoire ", repertoire_des_logs, " créé. ")
        else:
            print("Répertoire ", repertoire_des_logs, " existe déjà.")

        # Qq valeurs par défaut
        self.score = 0
        self.numero_de_tour = 0
        self.plage = '0-10'
        self.nombre = ''
        self.resultat_log = "0"

        Gtk.Window.__init__(self, title="Apprentissage des nombres")
        self.set_resizable(True)
        self.set_icon_from_file(self.dirBase  + "apropos.png")
        self.set_border_width(10)
        self.set_name('fieldset')
        self.nb_hasard = 20

        #
        # Creation de la grille
        grid = Gtk.Grid()
        grid.set_column_homogeneous(True)
        grid.set_column_spacing(6)
        grid.set_row_spacing(6)
        grid.set_row_homogeneous(False)

        # other buttons.
        # a new radiobutton with a label
        label_plage = Gtk.Label(label='Plage : ')
        grid.attach(label_plage, 0, 0, 1, 1)

        button_plage = Gtk.RadioButton(label="0-10")
        button_plage.connect("toggled", self.toggled)
        grid.attach(button_plage, 1, 0, 1, 1)

        possibilities = ['20-30', '30-40', '40-50', '50-60', '60-70', '70-80', '80-90', '90-100',
                         '60-80', '80-100']
        button_plages = [0] * len(possibilities)

        for i in range(len(possibilities)):
            # another radiobutton, in the same group as button1
            button_plages[i] = Gtk.RadioButton.new_from_widget(button_plage)
            button_plages[i].set_label(possibilities[i])
            button_plages[i].connect("toggled", self.toggled)
            button_plages[i].set_active(False)
            # attach the button
            grid.attach(button_plages[i], i + 2, 0, 1, 1)
        # Creation des buttons

        # Clavier numérique
        button_chiffres = [0,1,2,3,4,5,6,7,8,9]
        button_chiffres[i] = [0] * len(button_chiffres)

        for i in range(len(button_chiffres)):
            button_chiffres[i] = Gtk.Button(label=i)

        # Attache des boutons
        grid.attach(button_chiffres[0], 1, 2, 1, 1)

        grid.attach(button_chiffres[1], 0, 3, 1, 1)
        grid.attach(button_chiffres[2], 1, 3, 1, 1)
        grid.attach(button_chiffres[3], 2, 3, 1, 1)

        grid.attach(button_chiffres[4], 0, 4, 1, 1)
        grid.attach(button_chiffres[5], 1, 4, 1, 1)
        grid.attach(button_chiffres[6], 2, 4, 1, 1)

        grid.attach(button_chiffres[7], 0, 5, 1, 1)
        grid.attach(button_chiffres[8], 1, 5, 1, 1)
        grid.attach(button_chiffres[9], 2, 5, 1, 1)

        # Les signaux
        button_chiffres[0].connect("clicked", self.recup_chiffre,button_chiffres[0])
        button_chiffres[1].connect("clicked", self.recup_chiffre,button_chiffres[1])
        button_chiffres[2].connect("clicked", self.recup_chiffre,button_chiffres[2])
        button_chiffres[3].connect("clicked", self.recup_chiffre,button_chiffres[3])
        button_chiffres[4].connect("clicked", self.recup_chiffre,button_chiffres[4])
        button_chiffres[5].connect("clicked", self.recup_chiffre,button_chiffres[5])
        button_chiffres[6].connect("clicked", self.recup_chiffre,button_chiffres[6])
        button_chiffres[7].connect("clicked", self.recup_chiffre,button_chiffres[7])
        button_chiffres[8].connect("clicked", self.recup_chiffre,button_chiffres[8])
        button_chiffres[9].connect("clicked", self.recup_chiffre,button_chiffres[9])

        # Bouton son
        button_lire_son = Gtk.Button(label="ECOUTER SON")
        img = Gtk.Image.new_from_icon_name("media-playback-start", Gtk.IconSize.BUTTON )
        button_lire_son.set_image(img)
        button_lire_son.connect("clicked", self.ecouter_son)
        grid.attach(button_lire_son, 3, 2, 2, 2)

        # Valider
        self.button_valider = Gtk.Button(label="Valider")
        self.button_valider.set_sensitive(False)
        self.button_valider.connect("clicked", self.valider_nombre)
        self.button_valider.set_name("btn")
        grid.attach(self.button_valider, 3,4, 2, 1)

        # Bouton corriger
        button_corriger = Gtk.Button(label="Corriger")
        button_corriger.connect("clicked", self.corriger)
        grid.attach(button_corriger, 3, 5,2,1)

        # Zone affichage données
        self.label_nombre = Gtk.Label(label="")
        self.label_nombre.set_name('label_nombre')
        grid.attach(self.label_nombre, 5, 2, 4, 4)

        # Idenfication
        label_nom = Gtk.Label(label="Ton nom :")
        grid.attach(label_nom,9,2,1,1)
        self.entry_nom = Gtk.Entry()
        self.entry_nom.set_name('nom')
        grid.attach(self.entry_nom, 10, 2, 2, 1)

        # Message explicatif
        self.label_explication = Gtk.Label(label="Sélectionne une plage de données.\r\n \r\nPuis clique sur \"C'est parti !\" ")
        grid.attach(self.label_explication,9,3,3,2)


        # GO
        self.button_commencer = Gtk.Button(label="C'est parti !")
        self.button_commencer.connect("clicked", self.commencer_jeux)
        self.button_commencer.set_name("btn")
        grid.attach(self.button_commencer, 9, 5, 3, 1)

        # Barre de progression
        self.img = [0] * 10
        for i in range(10):
            self.img[i] = Gtk.Image.new_from_file(self.dirBase + 'nb-img/jaune.png')
            grid.attach(self.img[i], i, 9, 1, 1)

        # About
        button_about = Gtk.Button(label="About")
        button_about.connect("clicked", self.on_about)
        grid.attach(button_about, i +1, 9,2,1)


        self.add(grid)

    def recup_chiffre(self, widget, button):
        self.nombre = self.label_nombre.get_text()
        if self.label_nombre.get_text() == "00":
            self.nombre = ""
        else:
            self.nombre = self.nombre + button.get_label()

        self.label_nombre.set_text(self.nombre)

    def corriger(self, widget):
        if len(self.nombre) > 0:
            self.nombre = self.nombre[:-1]
            self.label_nombre.set_text(self.nombre)
        else:
            pass
    def log_des_reponses(self):

        now = datetime.datetime.now()
        now = now.strftime("%d/%m/%Y %H:%M:%S")
        name = self.entry_nom.get_text()
        log = str(now) + " : " + name  + " : "
        log = log + self.plage + " : " + self.nombre  + " : " +  self.label_nombre.get_text() + " : "
        log =  log + self.resultat_log + "\r\n"
        fichier_log = '/home/' +  os.environ.get('USER') + '/.primtux/nombresGtk/nombresGtk.log'
        fichier = open(fichier_log, "a")
        fichier.write(log)
        fichier.close()


    def commencer_jeux(self, button):
        if self.entry_nom.get_text() == "":
            self.mettre_son_nom()
        else:
            self.entry_nom.set_sensitive(False)
            self.button_valider.set_sensitive(True)
            self.label_nombre.set_text('')

            plage = self.plage.split('-')
            self.mini = int(plage[0])
            self.maxi = int(plage[1])
            #i = 0

            # On lance la racherche du premier nombre
            self.cherche_nombre()
            self.button_commencer.set_sensitive(False)


    def toggled(self, button):
        self.plage = button.get_label()

    def ecouter_son(self, widget):
        son = vlc.MediaPlayer(self.dirBase + "nb-mp3/" + str(self.nb_hasard) + ".mp3")
        son.play()
        time.sleep(1) # Nécessaire sinon pas de son...

    def cherche_nombre(self):
        self.nb_hasard = (random.choice(range(self.mini, self.maxi)))
        self.ecouter_son(self.nb_hasard)

    def valider_nombre(self, button):
        self.log_des_reponses()
        print('Num tour : ', self.numero_de_tour)
        print(self.label_nombre.get_text(), ' ---' , self.nb_hasard)
        if int(self.label_nombre.get_text()) == self.nb_hasard:
            self.resultat_log = "1"
            self.score += 1
            self.mettre_a_jour_image("vert")
        else:
            self.resultat_log = "0"
            self.mettre_a_jour_image("rouge")

        self.numero_de_tour += 1
        self.label_nombre.set_text('')

        print(self.score)
        if self.numero_de_tour < 10:
            self.cherche_nombre()
        else:
            print("Fini")
            self.fin_de_jeu_score()
            self.reinitialiser_valeur()

    def reinitialiser_valeur(self):
        self.button_commencer.set_sensitive(True)
        self.button_valider.set_sensitive(False)
        self.entry_nom.set_sensitive(False)
        self.numero_de_tour = 0
        self.score = 0

    def mettre_a_jour_image(self, couleur):
        i = self.numero_de_tour
        image = self.dirBase + 'nb-img/' + couleur + '.png'
        pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(filename=image, width=50, height=20,
                                                         preserve_aspect_ratio=False)
        self.img[i].set_from_pixbuf(pixbuf)

    def mettre_son_nom(self):
        message1 = "Attention, mettre son nom !"
        message2 = "Inscrit ton prénom avant de commencer ;) "
        dialog = Gtk.MessageDialog(
            transient_for=self,
            flags=0,
            message_type=Gtk.MessageType.WARNING,
            buttons=Gtk.ButtonsType.OK,
            text=message1,
        )
        dialog.format_secondary_text(message2)
        dialog.run()
        dialog.destroy()


    def fin_de_jeu_score(self):
        message1 = "Score"
        message2 = "Ton score est de " + str(self.score) + " sur 10."
        dialog = Gtk.MessageDialog(
            transient_for=self,
            flags=0,
            message_type=Gtk.MessageType.ERROR,
            buttons=Gtk.ButtonsType.OK,
            text=message1,
        )
        dialog.format_secondary_text(message2)
        dialog.run()
        dialog.destroy()


    def on_about(self, widget):
        """
        Fonction de la Boite de Dialogue About
        :param widget:
        :return:
        """
        # Recuperation n° de version
        print(__doc__)
        lignes = __doc__.split("\n")
        for l in lignes:
            if '__version__' in l:
                version = l[15:-1]
            if '__date__' in l:
                dateGtKBox = l[12:-1]

        authors = ["Cyrille BIOT"]
        documenters = ["Cyrille BIOT"]
        self.dialog = Gtk.AboutDialog()
        logo = GdkPixbuf.Pixbuf.new_from_file(self.dirBase + "apropos.png")
        if logo != None:
            self.dialog.set_logo(logo)
        else:
            print("A GdkPixbuf Error has occurred.")
        self.dialog.set_name("Gtk.AboutDialog")
        self.dialog.set_version(version)
        self.dialog.set_copyright("(C) 2020 Cyrille BIOT")
        self.dialog.set_comments("encoder-mots.py.\n\n" \
                                 "[" + dateGtKBox + "]")
        self.dialog.set_license("GNU General Public License (GPL), version 3.\n"
                                "This program is free software: you can redistribute it and/or modify\n"
                                "it under the terms of the GNU General Public License as published by\n"
                                "the Free Software Foundation, either version 3 of the License, or\n"
                                "(at your option) any later version.\n"
                                "\n"
                                "This program is distributed in the hope that it will be useful,\n"
                                "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                                "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                                "GNU General Public License for more details.\n"
                                "You should have received a copy of the GNU General Public License\n"
                                "along with this program.  If not, see <https://www.gnu.org/licenses/>\n")
        self.dialog.set_website("https://cbiot.fr")
        self.dialog.set_website_label("cbiot.fr")
        self.dialog.set_website("https://framagit.org/CyrilleBiot/nombreGtk")
        self.dialog.set_website_label("GIT ")
        self.dialog.set_authors(authors)
        self.dialog.set_documenters(documenters)
        self.dialog.set_translator_credits("Cyrille BIOT")
        self.dialog.connect("response", self.on_about_reponse)
        self.dialog.run()

    def on_about_reponse(self, dialog, response):
        """
        Fonction fermant la boite de dialogue About
        :param widget:
        :param response:
        :return:
        """
        self.dialog.destroy()

    def gtk_style(self):
        """
        Fonction definition de CSS de l'application
        Le fichier css : pendu-peda-gtk.css
        :return:
        """
        style_provider = Gtk.CssProvider()
        style_provider.load_from_path(self.dirBase + 'style.css')

        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

def main():
    """
    La boucle de lancement
    :return:
    """
    win = nbGtk()
    win.gtk_style()
    win.connect("destroy", Gtk.main_quit)
    win.move(10, 10)
    win.show_all()
    Gtk.main()

"""
 Boucle main()
"""
if __name__ == "__main__":
    # execute only if run as a script
    main()
