# nombresGtk

Cette version devrait être fonctionnelle. Reste à debugger. A tester.

Les logs sont situé dans le /home de l'user

```~/.primtux/nombresGtk/nombresGtk.log```

![screenshoot](./screenshoot1.png)

## Installation


### Depuis le paquet debian

L'installer

```$ wget https://framagit.org/CyrilleBiot/nombresGtk/-/blob/main/nombres-gtk_1.0_all.deb```

```# dpkg -i nombres-gtk_1.0_all.deb```

Le supprimer

```# dpkg -i nombres-gtk```


### Depuis le git

Juste cloner et lancer.


```git clone https://framagit.org/CyrilleBiot/nombreGtk.git```

```./source/encoder-mots.py```


## Dépendances

Les dépendances sont les suivantes. Le paquets debian les gère.

```python3-vlc, python3-gi```

## Fonts
Nécessite la police [alarm_clock.ttf] ( https://www.dafont.com/fr/alarm-clock.font )

Sera incluse dans le package debian

```fc-cache -f -v```

## TODO
Affichage des logs dans un textview

